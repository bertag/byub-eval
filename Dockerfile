FROM registry.gitlab.com/byuhbll/apps/openj9-jdk11

ARG JAR_FILE=target/byub-eval.jar
COPY ${JAR_FILE} /srv/app.jar
