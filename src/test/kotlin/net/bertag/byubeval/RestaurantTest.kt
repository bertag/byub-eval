package net.bertag.byubeval

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.time.DayOfWeek
import java.time.LocalTime

/**
 * Unit tests for the Restaurant class.
 */
class RestaurantTest {

    /**
     * Verifies that the class will correctly parse several different date/times, including a few edge cases.
     *
     * While this is not a comprehensive test, it does act as a nice sanity check to ensure that parsing has been
     * implemented correctly.
     */
    @ParameterizedTest
    @MethodSource("restaurantArgs")
    fun `should parse operating hours correctly`(
        input: List<String>,
        expected: Map<DayOfWeek, Set<Pair<LocalTime, LocalTime>>>
    ) {
        // GIVEN an list of operating hours represented as strings.
        // Provided by the input parameter.

        // WHEN a restaurant is constructed with those hours...
        val restaurant = Restaurant("My Restaurant", input)

        // THEN the formal definition of available times should match the expected value.
        assertEquals(expected, restaurant.hours);
    }

    companion object {
        @JvmStatic
        private fun restaurantArgs() = listOf(
            Arguments.of(
                listOf("Mon 8 am - 12 pm"),
                mapOf(
                    DayOfWeek.MONDAY to setOf(LocalTime.of(8, 0) to LocalTime.of(12, 0))
                )
            ),
            Arguments.of(
                listOf("Mon-Wed 8 am - 12 pm"),
                mapOf(
                    DayOfWeek.MONDAY to setOf(LocalTime.of(8, 0) to LocalTime.of(12, 0)),
                    DayOfWeek.TUESDAY to setOf(LocalTime.of(8, 0) to LocalTime.of(12, 0)),
                    DayOfWeek.WEDNESDAY to setOf(LocalTime.of(8, 0) to LocalTime.of(12, 0))
                )
            ),
            Arguments.of(
                listOf("Mon-Wed, Sun 8 am - 12 pm", "Fri 9 pm - 11:59 pm"),
                mapOf(
                    DayOfWeek.MONDAY to setOf(LocalTime.of(8, 0) to LocalTime.of(12, 0)),
                    DayOfWeek.TUESDAY to setOf(LocalTime.of(8, 0) to LocalTime.of(12, 0)),
                    DayOfWeek.WEDNESDAY to setOf(LocalTime.of(8, 0) to LocalTime.of(12, 0)),
                    DayOfWeek.FRIDAY to setOf(LocalTime.of(21, 0) to LocalTime.of(23, 59)),
                    DayOfWeek.SUNDAY to setOf(LocalTime.of(8, 0) to LocalTime.of(12, 0))
                )
            ),
            Arguments.of(
                listOf("Mon-Wed, Sun 8 am - 12 pm", "Fri, Sat 9 pm - 12 am"),
                mapOf(
                    DayOfWeek.MONDAY to setOf(LocalTime.of(8, 0) to LocalTime.of(12, 0)),
                    DayOfWeek.TUESDAY to setOf(LocalTime.of(8, 0) to LocalTime.of(12, 0)),
                    DayOfWeek.WEDNESDAY to setOf(LocalTime.of(8, 0) to LocalTime.of(12, 0)),
                    DayOfWeek.FRIDAY to setOf(LocalTime.of(21, 0) to LocalTime.MAX),
                    DayOfWeek.SATURDAY to setOf(LocalTime.of(21, 0) to LocalTime.MAX),
                    DayOfWeek.SUNDAY to setOf(LocalTime.of(8, 0) to LocalTime.of(12, 0))
                )
            ),
            Arguments.of(
                listOf("Mon-Wed, Sun 8 am - 12 pm", "Fri, Sat 9 pm - 12:30 am"),
                mapOf(
                    DayOfWeek.MONDAY to setOf(LocalTime.of(8, 0) to LocalTime.of(12, 0)),
                    DayOfWeek.TUESDAY to setOf(LocalTime.of(8, 0) to LocalTime.of(12, 0)),
                    DayOfWeek.WEDNESDAY to setOf(LocalTime.of(8, 0) to LocalTime.of(12, 0)),
                    DayOfWeek.FRIDAY to setOf(LocalTime.of(21, 0) to LocalTime.MAX),
                    DayOfWeek.SATURDAY to setOf(
                        LocalTime.MIDNIGHT to LocalTime.of(0, 30),
                        LocalTime.of(21, 0) to LocalTime.MAX
                    ),
                    DayOfWeek.SUNDAY to setOf(
                        LocalTime.MIDNIGHT to LocalTime.of(0, 30),
                        LocalTime.of(8, 0) to LocalTime.of(12, 0)
                    )
                )
            )
        )
    }

}