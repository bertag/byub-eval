package net.bertag.byubeval;

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.test.web.client.getForObject

/**
 * Integration tests for the API controller.
 *
 * @property app RESTful client for making requests to the application under test
 */
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class ApiControllerIT(@Autowired private val app: TestRestTemplate) {

    @Test
    fun `should retrieve list of open restaurants for a given datetime`() {
        // GIVEN a valid ISO-8601 datetime.
        val datetime = "2020-01-24T01:30"

        // WHEN the list of open restaurants is retrieved from the API for that time.
        val openRestaurants: List<String> = app.getForObject("/?datetime=2020-01-24T01:30")!!

        // THEN the list should include the expected values.
        // There is a restaurant (Sudachi) that closes at exactly 01:30 on Thursday night/Friday morning. Since we
        // consider hours to be a closed-open interval, Sudachi is appropriately excluded from the results.
        val expected = listOf("Naan 'N' Curry", "Marrakech Moroccan Restaurant")
        assertEquals(expected, openRestaurants)
    }

}
