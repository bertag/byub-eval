package net.bertag.byubeval

import java.time.DayOfWeek

/**
 * Extension method that calculates the inclusive list of days between this day and the other day.
 *
 * Unlike a normal range, this method works cyclically across week boundaries.  If the other day occurs earlier in the
 * calendar week than this day (e.g. FRIDAY.net.bertag.byubeval.listThrough(MONDAY) ), then the resulting list will quiely roll over into
 * the next week (e.g.: FRIDAY, SATURDAY, SUNDAY, MONDAY).
 *
 * @param other the last day to include in the list
 * @return the list of days between this day and the other day
 */
fun DayOfWeek.listThrough(other: DayOfWeek): List<DayOfWeek> {
    val offset = if (value < other.value) 0 else 7
    return (value..(other.value + offset)).map { it % 8 }.map(DayOfWeek::of)
}
