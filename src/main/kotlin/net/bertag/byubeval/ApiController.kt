package net.bertag.byubeval

import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDateTime

/**
 * Handler for API requests to this application.
 *
 * @property restaurants the list of restaurants and their operating hours
 */
@RestController
@RequestMapping(produces = [MediaType.APPLICATION_JSON_VALUE])
class ApiController(private val restaurants: List<Restaurant>) {

    /**
     * Given a date/time, returns the list of restaurants that are open at that time.
     *
     * @param datetime the date/time for which to check restaurant availability
     * @return the list of restaurants open at the given date/time
     */
    @GetMapping
    fun openRestaurantsApi(@RequestParam datetime: String): List<String> {
        // For the limited scope of this exercise, we're not worrying about input validation or error handling. Instead
        // we will just let Spring return the default error page for now.
        return restaurants.filter { it.isOpenAt(LocalDateTime.parse(datetime)) }.map { it.name }
    }

}