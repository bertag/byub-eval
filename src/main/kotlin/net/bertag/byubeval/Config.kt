package net.bertag.byubeval

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.io.ClassPathResource

/**
 * Configuration and component beans used by this application.
 */
@Configuration
class Config {

    /**
     * The Jackson object mapper used for JSON parsing.
     */
    @Bean
    fun objectMapper() = jacksonObjectMapper()

    /**
     * Restaurant data read from the provided source file.
     *
     * @return the restaurants and their operating hours
     */
    @Bean
    fun restaurants(): List<Restaurant> {
        val source = ClassPathResource("rest_hours.json").inputStream
        return objectMapper().readValue(source)
    }

}