package net.bertag.byubeval

import com.fasterxml.jackson.annotation.JsonIgnore
import java.time.DayOfWeek
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.format.DateTimeFormatter

/**
 * Model representing a restaurant and its operating hours.
 *
 * @property name the name of the restaurant
 * @property times the textual description of the restaurant's operating hours
 */
data class Restaurant(val name: String, val times: List<String>) {

    /**
     * The formal definition of the restaurant's operating hours
     */
    @JsonIgnore
    val hours: Map<DayOfWeek, Set<Pair<LocalTime, LocalTime>>>

    init {
        hours = times.flatMap { element ->
            // For each range of operating hours, split it into the day(s) of the week vs. the times/hours for that day.
            val boundary = element.indexOfFirst(Character::isDigit)
            val daysOfWeek = element.substring(0, boundary).let(::parseDays)
            val hours = element.substring(boundary)

            // Pair each day of the week to the operating hours for that day.
            daysOfWeek.flatMap { parseTime(it, hours) }
        }.groupBy({ it.first }, { it.second }).mapValues { it.value.toSet() }
        // We group by day of the week so that days with multiple ranges of operating hours (e.g: a restaurant remained
        // open over midnight the previous night and then reopens this evening) get those times bucketed together.
    }

    /**
     * Given a date/time, determines whether this restaurant is open at that moment.
     *
     * Operating hours are considered to be closed-open intervals, which is to say that a restaurant having the hours
     * "7 pm - 10 pm" opens at 19:00:00.000... local time and closes at 21:59:59.999... (the moment just prior to 10pm).
     *
     * @param datetime the date/time for which to determine availability
     * @return true if the restaurant is open; false otherwise
     */
    fun isOpenAt(datetime: LocalDateTime): Boolean {
        val time = datetime.toLocalTime()
        return hours[datetime.dayOfWeek]?.any { time >= it.first && time < it.second } ?: false
    }

    /**
     * Helper objects and functions used for converting the string-based representation of the operating hours into a
     * formal definition.
     */
    companion object {
        private val dayOfWeekParser = DateTimeFormatter.ofPattern("E")
        private val timeParser = DateTimeFormatter.ofPattern("h[:m] a")

        /**
         * For a known day of the week, parses the given text describing a range of times into a more formal structure.
         *
         * In a normal case, the resulting list will have just one element, which is indicative of a time range occurring
         * within a single calendar day (e.g.: Fri 7 pm - 11 pm).  However, if the end of the time range rolls over into the
         * next day (e.g.: Fri 7 pm - 1 am), then this list will contain 2 elements
         * (e.g.: Fri 7pm - 11:59pm, Sat 12 am - 1 am).
         *
         * @param day the day of the week for which this range of times begins
         * @param text the range of times
         * @return the day and time boundaries
         */
        private fun parseTime(day: DayOfWeek, text: String): List<Pair<DayOfWeek, Pair<LocalTime, LocalTime>>> {
            val range =
                text.split("-").map(String::trim).map(String::toUpperCase).map { timeParser.parse(it, LocalTime::from) }

            // If the hours extend beyond midnight (e.g. The Cheesecake Factory, Marrakech Moroccan Restaurant), we need
            // to do some additional parsing.  Finally, just prior to returning, filter out any entries where the start
            // and end time are the same value.
            return splitTimesByDay(
                day,
                range.first(),
                range.get(1)
            ).filter { it -> it.second.first != it.second.second }
        }

        /**
         * Helper method that handles the edge case where the published operating hours of a restaurant may actually run
         * over into the early hours of next day (e.g.: Fri 9 pm - 1 am) and splits them into a list of tuples corresponding
         * to the actual calendar hours for each day involved (e.g.: Fri 9 pm - 11:59 pm, Sat 12:00 am - 1 am).  If there is
         * no run over into the next day, then the resulting list will only contain a single element.
         *
         * @param startDay the day of week in which this time range started
         * @param startTime the starting time of day
         * @param endTime the ending time of day
         * @return the list of ranges, identified by which day of the week in which they ACTUALLY occur
         */
        private fun splitTimesByDay(
            startDay: DayOfWeek,
            startTime: LocalTime,
            endTime: LocalTime
        ): List<Pair<DayOfWeek, Pair<LocalTime, LocalTime>>> {
            return if (startTime < endTime) {
                // This is the normal situation; the start and end time occur on the same day so we just return the
                // single-element list.
                listOf(startDay to (startTime to endTime))
            } else {
                // This is the edge case; the end time actually occurs on the next day; return two elements in the list
                // (one for each day).
                listOf(
                    startDay to (startTime to LocalTime.MAX),
                    startDay + 1 to (LocalTime.MIDNIGHT to endTime)
                )
            }
        }

        /**
         * Parses each comma-delimited range of days (e.g.: "Mon-Wed, Sun") into the individual days of the week that it
         * describes (e.g.: Monday, Tuesday, Wednesday, Sunday).
         *
         * @param text the text to be parsed
         * @return the resulting list of individual days
         */
        private fun parseDays(text: String): List<DayOfWeek> {
            return text.split(",").map(String::trim).flatMap(::asListOfDays)
        }

        /**
         * Helper method that parses a range of days (e.g.: "Mon-Wed") into a list of individual days of the week that
         * it describes (e.g.: Monday, Tuesday, Wednesday). In the case that the given range is a single day
         * (e.g.: "Mon") then the list will simply contain that day as its sole element.
         *
         * @param range the textual range of days to be parsed
         * @return the list of individual days described by the range
         */
        private fun asListOfDays(range: String): List<DayOfWeek> {
            // Split the range along the hyphen.  Assuming properly formed input, we should now have a list with one or
            // two elements, depending on whether the input only describes a single day or a range of multiple days.
            val boundaries = range.split("-").map { dayOfWeekParser.parse(it, DayOfWeek::from) }

            // If we have more than one element, then call the DayOfWeek extension method to build the list of
            // individual days.  Otherwise, simply return the list as is.
            return if (boundaries.size > 1) boundaries[0].listThrough(boundaries[1]) else boundaries
        }
    }
}

