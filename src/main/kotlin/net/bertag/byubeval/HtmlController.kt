package net.bertag.byubeval

import org.springframework.http.MediaType
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.set
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping

/**
 * Handler for web requests to this application.
 */
@Controller
@RequestMapping(produces = [MediaType.TEXT_HTML_VALUE])
class HtmlController {

    /**
     * Displays a basic webpage front-end using Mustache templating.
     *
     * @param model the Mustache model to provide to the template
     * @return the name of the template to render
     */
    @GetMapping
    fun mainView(model: Model): String {
        model["title"] = "Restaurant Availability"
        return "index";
    }

}