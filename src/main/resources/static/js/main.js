$(document).ready(function () {

    $("#selection-form").submit(function (event) {
        event.preventDefault();
        fire_ajax_submit();
    });

});

function fire_ajax_submit() {

    // First, validate the date and time.  The HTML controls should input the right format, but we need to make sure
    // that both components are fully defined.
    if (!$("#date").val()) {
        $("#response").html("<p>It looks like the date wasn't understood; please check that you have defined a "
            + "month, day, and year.</p>");
    }
    else if (!$("#time").val()) {
        $("#response").html("<p>It looks like the time wasn't understood; please check that you have defined an "
            + "hour, minute, and whether this is AM/PM.</p>");
    } else {
        // Once validation passes, concat the date and time together into a full ISO datetime and make the API call.
        var datetime = $("#date").val() + "T" + $("#time").val();

        $.ajax({
            type: "GET",
            url: "/?datetime=" + datetime,
            accepts: { json: "application/json" },
            cache: false,
            timeout: 60000,
            success: function (data) {
                // On a successful response, populate the results with the list of restaurants.
                $("#response").html("<p>The following restaurants are open at " + datetime + ":</p><ul></ul>");
                $.each(data, function(index, name) {$("#response ul").append("<li>" + name + "</li>")});
            },
            error: function (e) {
                // On a error response, dishonor!  Dishonor on me, dishonor on my cow, dishonor on my whollllle family!
                $("#response").html("<p>Well this is embarrassing. Something has gone horribly wrong, and because this "
                    + "is just a coding exercise, I haven't implemented enough error handling to tell you what.</p>");
            }
        });
    }

}
