# Coding Exercise for BYU Broadcasting

Based on data in the included JSON file and the following assumptions, this application allows the user to specify a date and time and returns a list of restaurant names that are open.

## Usage

Included in this repository is a Docker image that you can download and run.

```bash
wget https://bitbucket.org/bertag/byub-eval/downloads/byub-eval.tar.gz
docker load < byuhb-eval.tar.gz
docker run --network=host --rm -d bertag/byub-eval
```

If you wish to verify/test, compile, and run the program from scratch, use the following commands to do so:

```bash
# For Debian/Ubuntu only...
sudo apt-get update
sudo apt-get install -y git maven

# For Red Hat/CentOS only...
sudo yum install -y git maven

# ------------------------------------------------------------------------------ 

git clone https://bitbucket.org/bertag/byub-eval.git
cd byub-eval

# Fair warning, this will download half the Internet to your `~/.m2` directory,
# so you might want to clean that out once you're done evaluating the program.
mvn verify spring-boot:run
```

This application includes both a simple front-end webpage and a backend API, both of which can be used to display the list of open restaurants.

### Front-end

The front-end is a single-page web application available at https://localhost:8080.  HTML 5 widgets are available for selecting a date and time.  The resulting form submission will use jQuery AJAX to call the API and populate the results list.

### API

The API is available at https://localhost:8080?datetime={ISO_DATETIME} but is only exposed if an `Accept: application/json` header is set.  The API requires a query param `datetime` which only accepts a properly formatted ISO-8601 date/time.
